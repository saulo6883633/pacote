# pacote

> Pacote de exemplo

## Instalação

```bash
composer require saulokn/pacote
```

## API

```php
    namespace saulokn\pacote;

    class Exemplo {
        /**
         * @return string nome
        */
        function nome();
    }
```

## Licença

MIT